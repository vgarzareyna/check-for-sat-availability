*** Settings ***
Library				SeleniumLibrary
Resource			Config.robot


*** Variables ***
${URL}					https://citas.sat.gob.mx/

${Inscripcion}				/html/body/jhi-main/div[2]/jhi-home/section/div/div[2]/div[2]/button
${Consulta}					/html/body/jhi-main/div[2]/jhi-home/section/div/div[2]/div[3]/button
${Persona Fisica}			//*[@id="mat-expansion-panel-header-2"]
${CURP IN}					//html/body/jhi-main/div[2]/jhi-datos-personales/section/div/div/div[2]/mat-accordion/mat-expansion-panel[3]/div/div/jhi-inscripcion-casos-especiales/form/div/div[1]/input
${Name IN}					//html/body/jhi-main/div[2]/jhi-datos-personales/section/div/div/div[2]/mat-accordion/mat-expansion-panel[3]/div/div/jhi-inscripcion-casos-especiales/form/div/div[2]/input
${Email1 IN}				//html/body/jhi-main/div[2]/jhi-datos-personales/section/div/div/div[2]/mat-accordion/mat-expansion-panel[3]/div/div/jhi-inscripcion-casos-especiales/form/div/div[3]/input
${Email2 IN}				//html/body/jhi-main/div[2]/jhi-datos-personales/section/div/div/div[2]/mat-accordion/mat-expansion-panel[3]/div/div/jhi-inscripcion-casos-especiales/form/div/div[4]/input
${Accept Conditions}		//html/body/jhi-main/div[2]/jhi-datos-personales/section/div/div/div[2]/mat-accordion/mat-expansion-panel[3]/div/div/jhi-inscripcion-casos-especiales/form/div/div[5]/mat-checkbox/label/span[1]

${Checkbox Physical Person}		/html/body/jhi-main/div[2]/jhi-datos-personales-cc/section/div/div[2]/div[2]/form/div[1]/mat-checkbox
#${Checkbox Physical Person}/html/body/jhi-main/div[2]/jhi-datos-personales-cc/section/div/div[2]/div[2]/form/div[1]/mat-checkbox/label/span[1]
${CURP IN (consulta)}		//*[@id="curp"]
${Email IN (consulta)}		//*[@id="email"]

*** Keywords ***
Fill Info
	maximize browser window
	Go to	${URL}
	Wait until element is visible		/html/body/div[1]/div[2]/div/mat-dialog-container/jhi-dialogo-inicial/mat-dialog-actions/button
	click element						/html/body/div[1]/div[2]/div/mat-dialog-container/jhi-dialogo-inicial/mat-dialog-actions/button
	Wait Until Element is Visible	${Inscripcion}
	Click Element    ${Inscripcion}
	Wait Until Element is Visible	${Persona Fisica}
	Click Element    ${Persona Fisica}
	Wait Until Element is Visible	${CURP IN}
	Input Text		${CURP IN}			${CURP}
	Input Text		${Name IN}			${Nombre}
	Input Text		${Email1 IN}		${Email}
	Input Text		${Email2 IN}		${Email}
	Wait Until Element is Visible	${Accept Conditions}
	Click Element   ${Accept Conditions}
	Set Focus to Element	//html/body/jhi-main/div[2]/jhi-datos-personales/section/div/div/div[2]/mat-accordion/mat-expansion-panel[3]/div/div/jhi-inscripcion-casos-especiales/form/div/div[6]/div/input

	## Enter the captcha manually

	${Timeout Time}=		Get Selenium Timeout
	Set Selenium Timeout	30 Seconds
	Wait Until Element is Visible	//*[@id="mat-select-0"]
	Set Selenium Timeout		${Timeout Time}

	Sleep						5 seconds

	# Tipo de Inscripción
	Click Element	//*[@id="mat-select-0"]
	CLick Element 	//html/body/div[2]/div[2]/div/div/div/mat-option[1]/span
	Wait Until Element is Visible	//html/body/div[2]/div[2]/div/mat-dialog-container/jhi-dialogo-servicios/mat-dialog-actions/button
	Sleep    1 seconds
	Click Element    //html/body/div[2]/div[2]/div/mat-dialog-container/jhi-dialogo-servicios/mat-dialog-actions/button

	# Estado
	Click Element 	//*[@id="mat-select-2"]
	press keys		None	NU
	wait until element is visible	//html/body/div[2]/div[2]/div/div/div/mat-option[19]/span
	Click Element 	//html/body/div[2]/div[2]/div/div/div/mat-option[19]/span

	# Municipio
	Click Element 	//*[@id="mat-select-4"]
	Click Element 	//*[@id="mat-option-38"]

	Sleep    5 seconds
	Element Should NOT Be Visible 	//*[@id="mat-dialog-title-1"]

	[Teardown]		Close Window



Check Virtual Queue
	maximize browser window
	Go to	https://citas.sat.gob.mx/consultaCita/datosPersonales
	Wait Until Element is Visible	/html/body/jhi-main/div[2]/jhi-datos-personales-cc/section/div/div[2]/div[2]/form/div[1]/mat-checkbox/label/span[1]
	Click Element 	/html/body/jhi-main/div[2]/jhi-datos-personales-cc/section/div/div[2]/div[2]/form/div[1]/mat-checkbox/label/span[1]
	Input Text		${CURP IN (consulta)}			${CURP}
	Input Text		${Email IN (consulta)}			${Email}
	Set Focus to Element	/html/body/jhi-main/div[2]/jhi-datos-personales-cc/section/div/div[2]/div[2]/form/div[2]/div[4]/input

	## Enter the captcha manually
	${Timeout Time}=		Get Selenium Timeout
	Set Selenium Timeout	30 Seconds
	Wait Until Element is Visible	//*[@id="mat-select-0"]
	Set Selenium Timeout		${Timeout Time}

	Sleep						5 seconds

	## Enter the token manually
	set focus to element			//*[@id="mat-input-4"]

	${Timeout Time}=		Get Selenium Timeout
	Set Selenium Timeout	120 Seconds
	Wait Until Element is Visible	//*[@id="mat-input-4"]
	Set Selenium Timeout		${Timeout Time}

	Sleep						5 seconds

	## Select text from the virtual thingy
	Wait until element is visible				/html/body/jhi-main/div[2]/jhi-listar-citas/div[3]/div/div/table/tbody/tr/td[4]
	${value}=  				Get value from		/html/body/jhi-main/div[2]/jhi-listar-citas/div[3]/div/div/table/tbody/tr/td[4]



	[Teardown]		Close Window